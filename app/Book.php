<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
   protected $fillable=['departments_id','b_name','b_code','writer_name','self_location', 'description', 'copys_number'];
}
