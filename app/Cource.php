<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cource extends Model
{
    protected $fillable=['departments_id','c_name','c_code','c_credit'];
}
