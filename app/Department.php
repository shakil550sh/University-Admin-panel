<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $fillable=['dpt_name','dpt_code','dpt_sname'];
     
}
