<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\Department;
use App\Student;
use App\Teacher;
use App\Cource;
use App\Book;

class AssignController extends Controller
{
    public function view()
    {
    	$dpt=Department::all();
    	return view('admin.department assign to techer.view',compact('dpt'));
    }
    public function dptassignstu(Request $request)
    {
    	$dt_id=$request->input('departments_id');
    	$student=Student::where('departments_id','=', $dt_id)->get();
    	return response()->json($student);
    }

    public function stuassigncource(Request $request)
    {
    	$dt_id=$request->input('departments_id');
    	$cource=Cource::where('departments_id','=', $dt_id)->get();
    	return response()->json($cource);
    }

    public function courceassigntecher(Request $request)
    {
    	$dt_id=$request->input('departments_id');
    	$teacher=Teacher::where('departments_id','=', $dt_id)->get();
    	return response()->json($teacher);
    }
  

}
