<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Http\Request;
use App\Department;
use App\Student;

class StudentController extends Controller
{
    public function view()
    {
    	$dpt=Department::all();
    	return view('admin.student.view',compact('dpt'));
    }
    public function viewStudent(Request $request)
    {
    	$dt_id=$request->input('departments_id');
    	$student=Student::where('departments_id','=', $dt_id)->get();
    	return response()->json($student);
    }
}
