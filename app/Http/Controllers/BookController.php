<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Department;
use App\Book;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $dpt=Department::all();
         $bb=Book::join('departments','books.departments_id','=','departments.id')->select('books.*','departments.dpt_name')->get();
        
         return view('LibarySection.viewbook',compact('dpt','bb'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $dpt=Department::all();
         $bok=Book::all();
        return view('LibarySection.addbook',compact('dpt','bok'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'b_name' => 'required:Book|max:255|min:3',
            'b_code' => 'required:Book|max:255|min:3',
            'writer_name' => 'required',
            'self_location' => 'required',
            'description' => 'required',
            'copys_number' => 'required',
        ]);
        $data=$request->all();
        Book::create($data);
        return back()->with('success','Book Add Successfully');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data=Book::find($id);
         $dpt=Department::all();
         return view('LibarySection.edit',compact('data','dpt'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data=Book::find($id);
        $new_data=$request->all();
        $data->Update($new_data);
        return back()->with('success','Update successfulluy');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data=Book::find($id);
        $data->delete();
        return back()->with('success','Data Delete Successfully');
    }
    
     public function dptBook(Request $request){
      $departments_id = $request->Input('departments_id');
      $find_book = Book::where('departments_id', '=', $departments_id)->get();
      return response()->json($find_book);
    }
}
