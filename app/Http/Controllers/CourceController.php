<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Department;
use App\Cource;

class CourceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cource=Cource::join('departments','cources.departments_id','=','departments.id')->select('cources.*','departments.dpt_name')->get();
        return view('cource.view',compact('cource'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $dpt = Department::all();
        return view('cource.create',compact('dpt'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $request->validate([
            'c_name' => 'required:Cource|min:4',
            'c_code' => 'required:Cource',
            'c_credit' => 'required',
            ]);
        $data=$request->all();
        Cource::create($data);
        return back()->with('success','Data insert successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data=Cource::find($id);
        $dpt=Department::all();
        return view('cource.edit',compact('data','dpt'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data=Cource::find($id);
        $new_data=$request->all();
        $data->update($new_data);
        return back()->with('success','data update successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data=Cource::find($id);
        $data->delete();
        return back()->with('success','Data Delete successfull');
    }
}
