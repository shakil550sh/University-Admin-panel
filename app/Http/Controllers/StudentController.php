<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Department;
use App\Student;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $stu=Student::join('departments','students.departments_id','=','departments.id')->select('students.*','departments.dpt_name')->get();
        // orderBy('id','desc')->paginate(5);
        
        return view('student.view',compact('stu'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $dpt=Department::all();
        return view('student.create',compact('dpt'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $department=Department::where('id',$request->departments_id)->first();
        $student=Student::where('departments_id',$request->departments_id)->get();
        $numrow=count($student)+1;
        if($numrow < 10){
            $std_id=$department->dpt_name.'-'.date('Y').'-'."00".$numrow;
        }
        elseif($numrow >=10 && $numrow<=99){
            $std_id=$department->dpt_name.'-'.date('Y').'-'."0".$numrow;

        }
        elseif($numrow<=100){
            $std_id=$department->dpt_name.'-'.date('Y').'-'.$numrow;
        }

        //  $request->validate([
        //     'st_name' => 'required|max:255|min:3',
        //     'std_id' => 'required',
        //     'stf_name' => 'required|min:4',
        //     'st_mobile' => 'required',
        //     'st_gender' => 'required',
        //     'st_address' => 'required',
            
        // ]);
        // $std=$request->all();
          $std=new Student;

        $std->st_name=$request->st_name;
        $std->departments_id=$request->departments_id;
        $std->std_id=$std_id;
        $std->stf_name=$request->stf_name;
        $std->st_mobile=$request->st_mobile;
        $std->st_gender=$request->st_gender;
        $std->st_address=$request->st_address;
        $std->save();
        return back()->with('success','Data insert successfully');



       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data=Student::find($id);
         $dpt=Department::all();
        return view('student.edit',compact('data','dpt'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data=Student::find($id);
        $new_data=$request->all();
        $data->update($new_data);
        return back()->with('success','Update Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data=Student::find($id);
        $data->delete();
        return back()->with('success','Delete Successfully');
    }
}
