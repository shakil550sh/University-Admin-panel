<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Teacher;
use App\Department;

class TeacherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $tech=Teacher::join('departments','teachers.departments_id','=','departments.id')->select('teachers.*','departments.dpt_name')->get();
        return view('teacher.view',compact('tech'));
    }
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $dpt=Department::all();
        return view('teacher.create',compact('dpt'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            't_name' => 'required|max:255|min:3',
            't_code' => 'required:teachers|min:4',
            't_mobile' => 'required',
            't_gender' => 'required',
            't_designation' => 'required',
            't_address' => 'required',
        ]);
        $data=$request->all();
        Teacher::create($data);
        return back()->with('success','Data insert successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
         
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data=Teacher::find($id);
        $dpt=Department::all();
        return view('teacher.edit',compact('data','dpt'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data=Teacher::find($id);
        $new_data=$request->all();
        $data->Update($new_data);
        return back()->with('success','update successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data=Teacher::find($id);
        $data->delete();
        return back()->with('success','delete successfully');
    }
}
