<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
     protected $fillable=['departments_id','st_name','stf_name','std_id','st_mobile','st_gender','st_address'];
}
