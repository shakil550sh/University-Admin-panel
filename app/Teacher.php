<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
    protected $fillable=['t_name','t_code','t_mobile','t_gender','t_designation','t_address','departments_id'];
}
