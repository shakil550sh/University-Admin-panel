<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->id();
             $table->integer('departments_id')->nullable();
            $table->string('st_name')->nullable();
            $table->string('stf_name')->nullable();
            $table->string('std_id')->nullable();
            $table->string('st_mobile')->nullable();
            $table->string('st_gender')->nullable();
            $table->string('st_address')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
