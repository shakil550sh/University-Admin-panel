@extends('frontend.master')
@section('title','Add Book')
@section('content')
<br><br><br>
	<div class="container main-wrapper form-control" align="center"><br>
@include('massage.massage')
	<form action="/book" method="POST">
		{{csrf_field()}}
		<div class="form-group">
			<label>Departments:-</label>
			<select name="departments_id">
				<option value="">Select a Department</option>
				@foreach($dpt as $data)
				<option value="{{$data->id}}">{{$data->dpt_name}}</option>
				@endforeach
			</select>			
		</div>
		<div class="form-group">
			<label>Book Name:-</label>
			<input type="text" name="b_name" placeholder="Book name">
		</div>
		<div class="form-group">
			<label>Book Code:-</label>
			<input type="text" name="b_code" placeholder="Book Code">
		</div>
		<div class="form-group">
			<label>Writter name:-</label>
			<input type="text" name="writer_name" placeholder="Writter name">
		</div>
		<div class="form-group">
			<label>Self Location:-</label>
			<input type="text" name="self_location" placeholder="Self Location">
		</div>		
		<div class="form-group">
			<label>Description:-</label>
			<textarea name="description" placeholder="Description"></textarea>			
		</div>
		<div class="form-group">
			<label>Total Number Of Copies:-</label>
			<input type="text" name="copys_number" placeholder="Total Number Of Copies">
		</div>
		<div class="form-group">
		<input type="submit" value="Submit">
	</div>		
	</form>
	<h1>View Books</h1>
	<table style="border: 2px solid red" align="center" border="1">
		<tr>
			<th>SI</th>			
			<th>Book code</th>
			<th>Book name</th>
			<th>Book Writter Name</th>
			<th>Available Copy</th>
			<th>Self Location</th>
			<th>Action</th>
		</tr>
		@foreach($bok as $key=>$data)			
		<tr>
			<th>{{++$key}}</th>
			<th>{{$data->b_name}}</th>
			<th>{{$data->b_code}}</th>
			<th>{{$data->writer_name}}</th>
			<th>{{$data->copys_number}}</th>
			<th>{{$data->self_location}}</th>
			<th><a href="/book/{{$data->id}}/edit">Edit</a> | <a href="/book/delete/{{$data->id}}">Delete</a></th>
		</tr>
		@endforeach
	</table>
</body>
</html>
@endsection