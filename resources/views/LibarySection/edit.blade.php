@extends('frontend.master')
@section('title','Add Book')
@section('content')
<br>
<br>
<br>


	<div class="container main-wrapper form-control" align="center">
<br>
<br>
<br>
@include('massage.massage')
	<form action="/book/{{$data->id}}" method="POST">
		{{csrf_field()}}

			<div class="form-group">
			<label>Departments:- </label>
			<select name="departments_id">
			
				@foreach($dpt as $d)
				@if($d->id == $data->departments_id)				
				<option value="{{$d->id}}" selected="">{{$d->dpt_name}}</option>
				@else
				<option value="{{$d->id}}">{{$d->dpt_name}}</option>
				@endif
				@endforeach
			</select>
			
		</div>

		<div class="form-group">
			<label>Book Name:-</label>
			<input type="text" name="b_name" placeholder="Book name" value="{{$data->b_name}}">
		</div>
		<div class="form-group">
			<label>Book Code:-</label>
			<input type="text" name="b_code" placeholder="Book Code" value="{{$data->b_code}}">
		</div>

		<div class="form-group">
			<label>Writter name:-</label>
			<input type="text" name="writer_name" placeholder="Writter name" value="{{$data->writer_name}}">
		</div>
		<div class="form-group">
			<label>Self Location:-</label>
			<input type="text" name="self_location" placeholder="Self Location" value="{{$data->self_location}}">
		</div>
		
		
		<div class="form-group">
			<label>Total Number Of Copies:-</label>
			<input type="text" name="copys_number" placeholder="Total Number Of Copies" value="{{$data->copys_number}}">
		</div>


		<div class="form-group">
		<input type="submit" value="Update">
	</div>

		
	</form>
</div>
</div>
@endsection