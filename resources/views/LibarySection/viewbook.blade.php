@extends('frontend.master')
@section('title','Add Book')
@section('content')
<br><br><br>
<div align="center" class="main-wrapper">
	<h1>View Books</h1>
	<h4><a href="/book/create">Add More Book</a></h4>

	<label>Select Departments:</label>
		<select name="departments_id" id="department" >
		<option value="">Select Departments</option>
		@foreach($dpt  as $dp) 
		<option value="{{$dp->id}}">{{$dp->dpt_name}}</option>
		@endforeach
	</select>
	<br>
	<br>
	<br>

	<table style="border: 2px solid red" align="center" border="1">
		<thead id="table-head">
			<tr>
			<th>SI</th>
			<th>Book code</th>
			<th>Book name</th>
			<th>Book Writter Name</th>
			<th>Available Copy</th>
			<th>Self Location</th>
			<th>Action</th>
		</tr>
		</thead>

		<tbody id="table-body">
		@foreach($bb as $key=>$data)	
		
			<tr>
			<th>{{++$key}}</th>
			<th>{{$data->b_name}}</th>
			<th>{{$data->b_code}}</th>
			<th>{{$data->writer_name}}</th>
			<th>{{$data->copys_number}}</th>
			<th>{{$data->self_location}}</th>
			<th><a href="/book/{{$data->id}}/edit">Edit</a> | <a href="/book/delete/{{$data->id}}">Delete</a></th>
		</tr>
		@endforeach	
		</tbody>		
		
	</table>	
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

<script type="text/javascript">
               
        $('#department').on('change',function(e){
            console.log(e);
            var departments_id= e.target.value;
            
            $.get('/json-dpt_book?departments_id=' + departments_id,function(data){
                console.log(data);
               $('#table-head').empty();
              $('#table-body').empty();
                
                $('#table-head').append("<tr><th>Sl No</th><th>Book code </th> <th>Book name </th> <th>Book Writter Name</th> <th>Available Copy </th><th>Self Location</th> <th>Action</th> </tr>");

                $.each(data, function(index, coursesObj){
                    $('#table-body').append( '<tr> <td>'+ coursesObj.id +'</td><td>'+ coursesObj.b_name +'</td><td>'+ coursesObj.b_code +'</td><td>'+ coursesObj.writer_name +'</td><td>'+ coursesObj.copys_number +'</td><td>'+ coursesObj.self_location +' </td> <td> Edit | Delete </td> </tr>');
                });

            });
            
        });

       
    </script>

@endsection