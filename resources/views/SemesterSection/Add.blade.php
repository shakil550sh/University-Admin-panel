@extends('frontend.master')
@section('title','Add Book')
@section('content')
<br><br><br>
	<div class="container main-wrapper form-control" align="center"><br>
@include('massage.massage')

    <div class="container">
         <div class="formclass">
             <h1>Semester Add</h1>

            <form action="/semesteradd" method="POST">
                {{ csrf_field() }}

                <label for="dpt">Select Department</label>
                <select name="dpt_name" id="dpt">
                        <option value=""> ==Select Department==</option>
                    @foreach($dpt as $data)
                        <option value="{{ $data->id }}">{{ $data->dpt_name }}</option>
                    @endforeach                    
                </select><br>

                <label for="sname">Semester Name:</label>
                <input type="text" name="s_name" id="sname" placeholder="Semester Name"><br>

                <label for="samesternumber">Semester Number:</label>
                <input type="text" name="samester_number" id="samesternumber" placeholder="samester Number"><br>

                <label for="startyear">Start Year:</label>
                <input type="Date" name="start_year" id="startyear" placeholder="Started year"><br>

                <label for="finished">Finished:</label>
                <input type="checkbox"  id="finished"> Not Graduate <input type="checkbox" id="finished" > Graduate <br>

                <label for="finsihyear">Graduate year:</label>
                <input type="Date" name="finish_year" id="finsihyear"><br>
                

                <input type="submit" value="Submit">                

            </form>

@endsection

	
