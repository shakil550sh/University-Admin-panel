@extends('frontend.master')
@section('title','Student view')
@section('content')
<div align="center" class="main-wrapper">
	<br>
		<br>
		<br>
		<br>
	
	<div class="form-group">
		<h4>Department</h4>

	<select name="departments_id" id="department">
		<option value="">Select</option>
		@foreach($dpt as $d)
		<option value="{{$d->id}}">{{$d->dpt_name}}</option>
		@endforeach
	</select>
</div>

	<div class="form-group">
		<select name="students_id" id="student">
		<option value="">please select a student</option>
		</select>
	</div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<!-- ace setting handler 
javascript code for show department waize student selectros-->

<script type="text/javascript">
	$('#department').on('change',function(e){
		
		console.log(e);
		var dot_id=e.target.value;
		$.get('/jsonStudents?departments_id='+dot_id,function(data){
			console.log(data);

			$('#student').empty();
			$('#student').append('<option value="" disable="true" selected="true">=== Select Student ===</option>');

			$.each(data, function(index, stdObj){
				$('#student').append('<option value="'+ stdObj.id +'">'+ stdObj.st_name +'</option>');
			});

		});
	});
</script>

@endsection

