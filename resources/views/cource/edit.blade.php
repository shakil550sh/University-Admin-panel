@extends('frontend.master')
@section('title','Add Cource')
@section('content')
<br>
<br>
<br>


	<div class="container main-wrapper form-control" align="center">
<br>
<br>
<br>
@include('massage.massage')
	<form action="/cources/{{$data->id}}" method="POST">
		{{csrf_field()}}
		<div class="form-group">
			<label>Select Departments</label>
			<select name="departments_id">
				
				@foreach($dpt as $d)
				@if($d->id == $data->departments_id)
				<option value="{{$d->id}}">{{$d->dpt_name}}</option>
				@else
				<option value="{{$d->id}}">{{$d->dpt_name}}</option>
				@endif
				@endforeach
			</select>
			
		</div>
		
		<div class="form-group">
			<label>Cource Name</label>
			<input type="text" name="c_name" placeholder="Cource name" value="{{$data->c_name}}">
		</div>
		<div class="form-group">
			<label>Cource Code</label>
			<input type="text" name="c_code" placeholder="Cource Code" value="{{$data->c_code}}">
		</div>
		<div class="form-group">
			<label>Cource Credit</label>
			<input type="text" name="c_credit" placeholder="Cource Credit" value="{{$data->c_credit}}">
		</div>	

		

		<div class="form-group">
		<input type="submit" value="Update">
	</div>

		
	</form>
</div>
</div>




@endsection