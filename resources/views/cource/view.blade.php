@extends('frontend.master')
@section('title','Teacher Profile')
@section('content')
	<div align="center" class="main-wrapper">
	<h1>View Cources</h1>
	<h4><a href="/cources/create">Add Cources</a></h4>

	<table style="border: 2px solid red" align="center" border="1">
		<tr>
			<th>SI</th>
			<th>Cource Department</th>
			<th>Cource Name</th>
			<th>Cource Code</th>
			<th>Cource Credit</th>
			<th>Action</th>
		</tr>
		@foreach($cource as $key=>$data)
			
		<tr>
			<th>{{++$key}}</th>
			<th>{{$data->dpt_name}}</th>
			<th>{{$data->c_name}}</th>
			<th>{{$data->c_code}}</th>
			<th>{{$data->c_credit}}</th>
			<th><a href="/cources/{{$data->id}}/edit">Edit</a> | <a href="/cources/delete/{{$data->id}}">Delete</a></th>
		</tr>
		@endforeach
	
	</table>
	

	</div>
	

</body>
</html>
@endsection