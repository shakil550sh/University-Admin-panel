@extends('frontend.master')
@section('title','Add Department')
@section('content')
<br>
<br>
<br>


	<div class="container main-wrapper" align="center">
		<br>
<br>
@include('massage.massage')
<br>
	<form action="/departments/{{$data->id}}" method="POST">
		{{csrf_field()}}
		<div>
			<label>Departments Name</label>
			<input type="text" name="dpt_name" placeholder="departments name" value="{{$data->dpt_name}}">
		</div>
		<div>
			<label>Departments Short Name</label>
			<input type="text" name="dpt_sname" placeholder="departments Short Name" value="{{$data->dpt_sname}}">
		</div>

		<div>
			<label>Departments Code</label>
			<input type="text" name="dpt_code" placeholder="departments code" value="{{$data->dpt_code}}">
		</div>

		
		<input type="submit" value="Update">

		
	</form>
</div>
</div>




@endsection