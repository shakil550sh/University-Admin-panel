@extends('frontend.master')
@section('title','Department View')
@section('content')
	<div align="center" class="main-wrapper">
	<h1>View Departments</h1>
	<h4><a href="/departments/create">Add More Departments</a></h4>

	<table style="border: 2px solid red" align="center" border="1">
		<tr>
			<th>SI</th>
			<th>Department Name</th>
			<th>Department Short Name</th>
			<th>Department CODE</th>
			<th>Action</th>
		</tr>

			
			@foreach($dpt as $key=>$data)
		<tr>
			<th>{{++$key}}</th>
			<th>{{$data->dpt_name}}</th>
			<th>{{$data->dpt_sname}}</th>
			<th>{{$data->dpt_code}}</th>
			<th> <button><a href="/departments/{{$data->id}}/edit">Edit</a> </button><button> <a href="/departments/delete/{{$data->id}}" onclick="return confirm('are you sure?')" >Delete</a></button></th>
		</tr>
		@endforeach
	</table>
	{{$dpt->links()}} 
	</div>
	

</body>
</html>
@endsection