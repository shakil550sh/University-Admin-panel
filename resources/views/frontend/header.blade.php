<!DOCTYPE html>
<html lang="en-us">
  <head>
    


    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Admin panel | @yield('title')</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">


    <!-- lib-->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:400,100,100italic,300,300italic,400italic,700,700italic,900,900italic">
    <!--
    link(rel='stylesheet' href='assets/stylesheets/ionicons.css')
    link(rel='stylesheet' href='assets/stylesheets/font-awesome.css')
    link(rel='stylesheet' href='assets/stylesheets/weather-icons.min.css')
    link(rel='stylesheet' href='assets/stylesheets/animate.css')
    link(rel='stylesheet' href='assets/stylesheets/glyphicon.css') 
    
    -->

    <!--
    plugin
    link(rel='stylesheet' href='assets/stylesheets/plugin/bootstrap-table.css')
    link(rel='stylesheet' href='assets/stylesheets/plugin/nifty-modal.css')
    link(rel='stylesheet' href='assets/stylesheets/plugin/jquery.bootstrap-touchspin.css')
    link(rel='stylesheet' href='assets/stylesheets/plugin/select2.css')
    link(rel='stylesheet' href='assets/stylesheets/plugin/multi-select.css')
    link(rel='stylesheet' href='assets/stylesheets/plugin/ladda.min.css')
    link(rel='stylesheet' href='assets/stylesheets/plugin/daterangepicker.css')
    link(rel='stylesheet' href='assets/stylesheets/plugin/jquery.timepicker.css')
    link(rel="stylesheet" href="assets/stylesheets/plugin/jqvmap.css")
    link(rel="stylesheet" href="assets/stylesheets/plugin/bootstrap-submenu.css")
    link(rel="stylesheet" href="assets/stylesheets/plugin/code.css")
    link(rel="stylesheet" href="assets/stylesheets/plugin/jquery.dataTables.css")
    link(rel="stylesheet" href="assets/stylesheets/plugin/dataTables.bootstrap.css")
    link(rel="stylesheet" href="assets/stylesheets/plugin/jquery.gridster.css")
    link(rel="stylesheet" href="assets/stylesheets/plugin/summernote.css")
    link(rel="stylesheet" href="assets/stylesheets/plugin/bootstrap-markdown.min.css")
    link(rel="stylesheet" href="assets/stylesheets/plugin/bootstrap-select.css")
    link(rel="stylesheet" href="assets/stylesheets/plugin/asColorPicker.css")
    link(rel="stylesheet" href="assets/stylesheets/plugin/bootstrap-datepicker.css")
    link(rel="stylesheet" href="assets/stylesheets/plugin/jquery-labelauty.css")
    link(rel="stylesheet" href="assets/stylesheets/plugin/owl.carousel.min.css")
    link(rel="stylesheet" href="assets/stylesheets/plugin/owl.theme.default.min.css")
    
    -->

    <!-- Theme-->
    <!-- Concat all lib & plugins css-->
    <link id="mainstyle" rel="stylesheet" href="{{asset('stylesheets/theme-libs-plugins.css')}}">
    <link id="mainstyle" rel="stylesheet" href="{{asset('stylesheets/skin.css')}}">

    <!-- Demo only-->
    <link id="mainstyle" rel="stylesheet" href="{{asset('stylesheets/demo.css')}}">

    <!-- This page only-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries--><!--[if lt IE 9]>
    <script src="assets/scripts/lib/html5shiv.js"></script>
    <script src="assets/scripts/lib/respond.js"></script><![endif]-->
    <script src="{{asset('scripts/lib/modernizr-custom.js')}}"></script>
    <script src="{{asset('scripts/lib/respond.js')}}"></script>
   
  </head>

  <body class="orchid  ">

    <!-- #SIDEMENU-->
    <div class="mainmenu-block">
      <!-- SITE MAINMENU-->
      <nav class="menu-block">
        <ul class="nav">
          <li class="nav-item mainmenu-user-profile"><a href="user-profile.html">
              <div class="circle-box"><img src="{{asset('images/face/shakil.')}}jpg" alt="">
                <div class="dot dot-success"></div>
              </div>
              <div class="menu-block-label"><b> Md Shakil Hussain</b><br>Managing Director</div></a></li>
        </ul>
        <div class="p-a-2"><span class="small pull-xs-right">This Month Earnings</span>
          <canvas id="demo-bar-dark-chart"></canvas>
        </div>
        <ul class="nav">
          <li class="nav-item"><a class="nav-link" href="/"><i class="icon ion-home"></i>
              <div class="menu-block-label">Home</div></a></li>

              <li class="nav-item"><a class="nav-link" href="/studentview"><i class="icon ion-home"></i>
              <div class="menu-block-label">Student full view</div></a></li>

              <li class="nav-item"><a class="nav-link" href="/assignview"><i class="icon ion-home"></i>
              <div class="menu-block-label">Student Assign</div></a></li>


                  <!-- Department section -->
                  <li class="menu-block-has-sub nav-item"><a class="nav-link" href="#"><i class="icon ion-monitor"></i>      
            <div class="menu-block-label">Department<span class="label label-success">dpt</span></div></a>
            <ul class="nav menu-block-sub">
              <li class="nav-item"><a class="nav-link" href="/departments/create">Add departments</a></li>
              <li class="nav-item"><a class="nav-link" href="/departments">View departments<span class="label label-success"></span></a></li>
            </ul>
          </li>
              <!-- end Department section -->
               

              <!-- Techer section -->
                  <li class="menu-block-has-sub nav-item"><a class="nav-link" href="#"><i class="icon ion-monitor"></i>      
            <div class="menu-block-label">teacher<span class="label label-success">teacher</span></div></a>
            <ul class="nav menu-block-sub">
              <li class="nav-item"><a class="nav-link" href="/teacher/create">Add Teacher</a></li>
              <li class="nav-item"><a class="nav-link" href="/teacher">View Teacher<span class="label label-success"></span></a></li>
            </ul>
          </li>
              <!-- end techer section -->

        
              <!-- cource -->
                <li class="menu-block-has-sub nav-item"><a class="nav-link" href="#"><i class="icon ion-monitor"></i>      
            <div class="menu-block-label">Cource<span class="label label-success">Cource</span></div></a>
            <ul class="nav menu-block-sub">
              <li class="nav-item"><a class="nav-link" href="/cources/create">Add Cources</a></li>
              <li class="nav-item"><a class="nav-link" href="/cources">View Cources<span class="label label-success"></span></a></li>
            </ul>
          </li>
         <!--  cource end -->


        <!--  Student Start -->        

               <li class="menu-block-has-sub nav-item"><a class="nav-link" href="#"><i class="icon ion-monitor"></i>      
            <div class="menu-block-label">Student<span class="label label-success">student</span></div></a>
            <ul class="nav menu-block-sub">
              <li class="nav-item"><a class="nav-link" href="/student/create">Add Student</a></li>
              <li class="nav-item"><a class="nav-link" href="/student">View Students<span class="label label-success"></span></a></li>
            </ul>
          </li>
 <!--  Student end -->
    <!--  Book Section -->        

               <li class="menu-block-has-sub nav-item"><a class="nav-link" href="#"><i class="icon ion-monitor"></i>      
            <div class="menu-block-label">Book Section<span class="label label-success"></span></div></a>
            <ul class="nav menu-block-sub">
              <li class="nav-item"><a class="nav-link" href="/book/create">Add Book</a></li>
              <li class="nav-item"><a class="nav-link" href="/book">View Book<span class="label label-success"></span></a></li>
            </ul>
          </li>
 <!--  Book Section End -->

          <li class="menu-block-has-sub nav-item"><a class="nav-link" href="#"><i class="icon ion-monitor"></i>      
          	<div class="menu-block-label">Dashboard<span class="label label-success">new</span></div></a>
            <ul class="nav menu-block-sub">
              <li class="nav-item"><a class="nav-link" href="dashboard-1.html">Dashboard Basic</a></li>
              <li class="nav-item"><a class="nav-link" href="dashboard-2.html">Game Dashboard<span class="label label-success">new</span></a></li>
            </ul>
          </li>
          <!--li.header GENERAL-->
         <!--  <li class="menu-block-has-sub nav-item"><a class="nav-link" href="#"><i class="icon ion-ios-grid-view-outline"></i>
              <div class="menu-block-label">Layouts</div></a>
            <ul class="nav menu-block-sub">
              <li class="nav-item"><a class="nav-link" href="layout-drawer.html">Drawer layout</a></li>
              <li class="nav-item"><a class="nav-link" href="layout-drawer-color-header.html">Drawer with header</a></li>
              <li class="nav-item"><a class="nav-link" href="layout-basic.html">Basic layout</a></li>
              <li class="nav-item"><a class="nav-link" href="layout-left-sidebar.html">Left Sidebar</a></li>
              <li class="nav-item"><a class="nav-link" href="layout-left-fixed-sidebar.html">Left Fixed Sidebar</a></li>
              <li class="nav-item"><a class="nav-link" href="layout-right-sidebar.html">Right Sidebar</a></li>
              <li class="nav-item"><a class="nav-link" href="layout-right-fixed-sidebar.html">Right Fixed Sidebar</a></li>
              <li class="nav-item"><a class="nav-link" href="layout-menu-collapsed.html">Menu Collapsed</a></li>
              <li class="nav-item"><a class="nav-link" href="layout-menu-expended.html">Menu Expended</a></li>
              <li class="nav-item"><a class="nav-link" href="layout-header.html">Header Styles</a></li>
            </ul>
          </li> -->
         <!--  <li class="menu-block-has-sub nav-item"><a class="nav-link" href="#"><i class="icon ion-grid"></i>
              <div class="menu-block-label">Tables</div></a>
            <ul class="nav menu-block-sub">
              <li class="nav-item"><a class="nav-link" href="table-ui.html">Table UI</a></li>
              <li class="nav-item"><a class="nav-link" href="table-basic.html">Table Basic</a></li>
              <li class="nav-item"><a class="nav-link" href="table-bootstrap.html">Table Bootstrap</a></li>
              <li class="nav-item"><a class="nav-link" href="table-datatables.html">Table Datatables</a></li>
            </ul>
          </li> -->
         <!--  <li class="menu-block-has-sub nav-item"><a class="nav-link" href="#"><i class="icon ion-toggle"></i>
              <div class="menu-block-label">Form</div></a>
            <ul class="nav menu-block-sub">
              <li class="nav-item"><a class="nav-link" href="form-basic.html">Form basic</a></li>
              <li class="nav-item"><a class="nav-link" href="form-advanced.html">Form advanced</a></li>
              <li class="nav-item"><a class="nav-link" href="form-layout.html">Form layout</a></li>
              <li class="nav-item"><a class="nav-link" href="form-validation.html">Form validation</a></li>
            </ul>
          </li> -->
         <!--  <li class="menu-block-has-sub nav-item"><a class="nav-link" href="#"><i class="icon ion-cube"></i>
              <div class="menu-block-label">UI Elements</div><span class="label label-success">new</span></a>
            <ul class="nav menu-block-sub">
              <li class="nav-item"><a class="nav-link" href="ui-button.html">Buttons</a></li>
              <li class="nav-item"><a class="nav-link" href="ui-chart.html">Chart</a></li>
              <li class="nav-item"><a class="nav-link" href="ui-color.html">Color</a></li>
              <li class="nav-item"><a class="nav-link" href="ui-dropdowns.html">Dropdowns</a></li>
              <li class="nav-item"><a class="nav-link" href="ui-files.html">Files</a></li>
              <li class="nav-item"><a class="nav-link" href="ui-labels-badges.html">Badges & Labels</a></li>
              <li class="nav-item"><a class="nav-link" href="ui-messages.html">Messages</a></li>
              <li class="nav-item"><a class="nav-link" href="ui-modal.html">Modal</a></li>
              <li class="nav-item"><a class="nav-link" href="ui-navigation.html">Navigation</a></li>
              <li class="nav-item"><a class="nav-link" href="ui-notifications.html">Notication</a></li>
              <li class="nav-item"><a class="nav-link" href="ui-panel.html">Panel</a></li>
              <li class="nav-item"><a class="nav-link" href="ui-progress.html">Progress</a></li>
              <li class="nav-item"><a class="nav-link" href="ui-tab.html">Tabs</a></li>
              <li class="nav-item"><a class="nav-link" href="ui-timeline.html">Timeline</a></li>
              <li class="nav-item"><a class="nav-link" href="ui-tooltips-popovers.html">Tooltips & Popovers</a></li>
              <li class="nav-item"><a class="nav-link" href="ui-editor.html">Editor</a></li>
              <li class="nav-item"><a class="nav-link" href="ui-type.html">type</a></li>
              <li class="nav-item"><a class="nav-link" href="ui-widget.html">Widget</a></li>
              <li class="nav-item"><a class="nav-link" href="ui-scrollers.html">Scrollers</a></li>
              <li class="nav-item"><a class="nav-link" href="ui-broadcard.html">Broadcard<span class="label label-success">new</span></a></li>
            </ul>
          </li> -->
          <!--li.header.nav-item APP-->
<!--           <li class="menu-block-has-sub nav-item"><a class="nav-link" href="#"><i class="icon ion-document"></i>
              <div class="menu-block-label">Page view</div></a>
            <ul class="nav menu-block-sub">
              <li class="nav-item"><a class="nav-link" href="page-tasks.html">Tasks</a></li>
              <li class="nav-item"><a class="nav-link" href="page-project-details.html">Project Details</a></li>
              <li class="nav-item"><a class="nav-link" href="page-media-lib.html">Media Library</a></li>
              <li class="nav-item"><a class="nav-link" href="page-tracker.html">Tracker</a></li>
              <li class="nav-item"><a class="nav-link" href="page-mail.html">Mail</a></li>
              <li class="nav-item"><a class="nav-link" href="page-inbox.html">Inbox</a></li>
              <li class="nav-item"><a class="nav-link" href="page-inbox-2.html">Inbox 2</a></li>
            </ul>
          </li> -->
         <!--  <li class="menu-block-has-sub nav-item"><a class="nav-link" href="#"><i class="icon ion-android-people"></i>
              <div class="menu-block-label">User</div></a>
            <ul class="nav menu-block-sub">
              <li class="nav-item"><a class="nav-link" href="user-login.html">Login</a></li>
              <li class="nav-item"><a class="nav-link" href="user-login-2.html">Login 2</a></li>
              <li class="nav-item"><a class="nav-link" href="user-login-3.html">Login 3</a></li>
              <li class="nav-item"><a class="nav-link" href="user-register.html">Register</a></li>
              <li class="nav-item"><a class="nav-link" href="user-register-2.html">Register 2</a></li>
              <li class="nav-item"><a class="nav-link" href="user-register-3.html">Register 3</a></li>
              <li class="nav-item"><a class="nav-link" href="user-register-4.html">Register 4</a></li>
              <li class="nav-item"><a class="nav-link" href="user-lists.html">Users Lists</a></li>
              <li class="nav-item"><a class="nav-link" href="user-profile.html">User Profile</a></li>
              <li class="nav-item"><a class="nav-link" href="user-profile-2.html">User Profile 2</a></li>
              <li class="nav-item"><a class="nav-link" href="user-profile-3.html">User Profile 3</a></li>
            </ul>
          </li> -->
          <!--li.header.nav-item user interface-->
        <!--   <li class="menu-block-has-sub nav-item"><a class="nav-link" href="#"><i class="icon ion-android-funnel"></i>
              <div class="menu-block-label">Menu Level</div></a>
            <ul class="nav menu-block-sub">
              <li class="nav-item"><a class="nav-link" href="#">Commas</a></li>
              <li class="nav-item menu-block-has-sub"><a href="#">Neversa</a>
                <ul class="nav menu-block-sub">
                  <li class="nav-item"><a class="nav-link" href="#">Incurred</a></li>
                  <li class="nav-item"><a class="nav-link" href="#">Preparation</a></li>
                </ul>
              </li>
              <li class="nav-item"><a class="nav-link" href="#">Country</a></li>
            </ul>
          </li> -->
        </ul>
        <!-- END SITE MAINMENU-->
      </nav>
    </div>