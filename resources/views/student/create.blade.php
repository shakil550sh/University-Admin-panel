@extends('frontend.master')
@section('title','Add Student')
@section('content')
<br>
<br>
<br>


	<div class="container main-wrapper form-control" align="center">
<br>
<br>
<br>
@include('massage.massage')
	<form action="/student" method="POST">
		{{csrf_field()}}
				<div class="form-group">
			<label>Select Departments</label>
			<select name="departments_id">
				<option value="">Select a Department</option>
				@foreach($dpt as $data)
				<option value="{{$data->id}}">{{$data->dpt_name}}</option>
				@endforeach
			</select>			
		</div>

		<div class="form-group">
			<label>Student Name</label>
			<input type="text" name="st_name" placeholder="Student name">
		</div>
		{{-- <div class="form-group">
			<label>Student Id</label>
			<input type="text" name="std_id" placeholder="Student ID">
		</div> --}}
		
		
		<div class="form-group">
			<label>Student Fathers Name</label>
			<input type="text" name="stf_name" placeholder="Student Father Name">
		</div>

		<div class="form-group">
			<label>Student Mobile</label>
			<input type="text" name="st_mobile" placeholder="Student mobile number">
		</div>
		<div class="form-group">
			<label>Student Gender</label>
			<input type="text" name="st_gender" placeholder="Student Gender">
		</div>
		
		<div class="form-group">
			<label>Student Address</label>
			<textarea name="st_address" placeholder="Student Address"></textarea>
			
		</div>

		<div class="form-group">
		<input type="submit" value="save">
	</div>

		
	</form>
</div>
</div>




@endsection