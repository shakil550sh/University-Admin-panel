@extends('frontend.master')
@section('title','Add Student')
@section('content')
<br>
<br>
<br>


	<div class="container main-wrapper form-control" align="center">
<br>
<br>
<br>
@include('massage.massage')
	<form action="/student/{{$data->id}}" method="POST">
		{{csrf_field()}}
				<div class="form-group">
			<label>Select Departments</label>
			<select name="departments_id">

				

				@foreach($dpt as $d)
				@if($d->id == $data->departments_id)

				<option value="{{$d->id}}" selected >{{$d->dpt_name}}</option>
				@else
				<option value="{{$d->id}}">{{$d->dpt_name}}</option>
				@endif
				@endforeach
				
			</select>			
		</div>

		<div class="form-group">
			<label>Student Name</label>
			<input type="text" name="st_name" placeholder="Student name" value="{{$data->st_name}}">
		</div>
		<div class="form-group">
			<label>Student id</label>
			<input type="text" name="std_id" placeholder="Student Id" value="{{$data->std_id}}">
		</div>
		
		<div class="form-group">
			<label>Student Fathers Name</label>
			<input type="text" name="stf_name" placeholder="Student Father Name" value="{{$data->stf_name}}">
		</div>

		<div class="form-group">
			<label>Student Mobile</label>
			<input type="text" name="st_mobile" placeholder="Student mobile number" value="{{$data->st_mobile}}">
		</div>
		<div class="form-group">
			<label>Student Gender</label>
			<input type="text" name="st_gender" placeholder="Student Gender" value="{{$data->st_gender}}">
		</div>
		
		<div class="form-group">
			<label>Student Address</label>
			<textarea name="st_address" placeholder="Student Address">{{$data->st_address}}</textarea>
			
		</div>

		<div class="form-group">
		<input type="submit" value="Update">
	</div>

		
	</form>
</div>
</div>




@endsection