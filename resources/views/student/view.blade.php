@extends('frontend.master')
@section('title','Student Profile')
@section('content')
	<div align="center" class="main-wrapper">
		
	<h1>View Student Profile</h1>
	<h4><a href="/student/create">Add More Student</a></h4>

	<table style="border: 2px solid red" align="center" border="1">
		<tr>
			<th>SI</th>
			<th>Student Department</th>
			<th>Student name</th>
			<th>Student Id</th>			
			<th>Student Father Name</th>
			<th>Student mobile</th>
			<th>Student gender</th>
			<th>teacher address</th>
			
			<th>Action</th>
		</tr>
		@foreach($stu as $key=>$data)
			
		<tr>
			<th>{{++$key}}</th>
			<th>{{$data->dpt_name}}</th>
			<th>{{$data->st_name}}</th>
			<th>{{$data->std_id}}</th>
			<th>{{$data->stf_name}}</th>
			<th>{{$data->st_mobile}}</th>
			<th>{{$data->st_gender}}</th>
		
			<th>{{$data->st_address}}</th>
		
			<th><a href="/student/{{$data->id}}/edit">Edit</a> | <a href="/student/delete/{{$data->id}}">Delete</a></th>
		</tr>
		@endforeach
	
	</table>
	

	</div>
	

</body>
</html>
@endsection