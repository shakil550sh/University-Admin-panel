@extends('frontend.master')
@section('title','Add Department')
@section('content')
<br>
<br>
<br>


	<div class="container main-wrapper form-control" align="center">
<br>
<br>
<br>
@include('massage.massage')
	<form action="/teacher" method="POST">
		{{csrf_field()}}
		<div class="form-group">
			<label>Teacher Name</label>
			<input type="text" name="t_name" placeholder="Teacher name">
		</div>
		<div class="form-group">
			<label>Teacher Code</label>
			<input type="text" name="t_code" placeholder="Teacher Code">
		</div>

		<div class="form-group">
			<label>Teacher Mobile</label>
			<input type="text" name="t_mobile" placeholder="Teacher mobile number">
		</div>
		<div class="form-group">
			<label>Teacher Gender</label>
			<input type="text" name="t_gender" placeholder="Teacher Gender">
		</div>
		<div class="form-group">
			<label>Teacher Designation</label>
			<input type="text" name="t_designation" placeholder="Teacher Designation">
		</div>
		<div class="form-group">
			<label>Address</label>
			<textarea name="t_address" placeholder="Teacher Address"></textarea>
			
		</div>

		<div class="form-group">
			<label>Select Departments</label>
			<select name="departments_id">
				<option value="">Select a Department</option>
				@foreach($dpt as $data)
				<option value="{{$data->id}}">{{$data->dpt_name}}</option>
				@endforeach
			</select>
			
		</div>

		<div class="form-group">
		<input type="submit" value="save">
	</div>

		
	</form>
</div>
</div>




@endsection