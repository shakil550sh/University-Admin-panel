@extends('frontend.master')
@section('title','Add Department')
@section('content')
<br>
<br>
<br>


	<div class="container main-wrapper form-control" align="center">
<br>
<br>
<br>
@include('massage.massage')
	<form action="/teacher/{{$data->id}}" method="POST">
		{{csrf_field()}}
				<div class="form-group">
			<label>Select Departments</label>
			<select name="departments_id">
			
				@foreach($dpt as $d)
				@if($d->id == $data->departments_id)				
				<option value="{{$d->id}}" selected="">{{$d->dpt_name}}</option>
				@else
				<option value="{{$d->id}}">{{$d->dpt_name}}</option>
				@endif
				@endforeach
			</select>
			
		</div>


		<div class="form-group">
			<label>Teacher Name</label>
			<input type="text" name="t_name" placeholder="Teacher name" value="{{$data->t_name}}">
		</div>
		<div class="form-group">
			<label>Teacher Code</label>
			<input type="text" name="t_code" placeholder="Teacher Code" value="{{$data->t_code}}">
		</div>

		<div class="form-group">
			<label>Teacher Mobile</label>
			<input type="text" name="t_mobile" placeholder="Teacher mobile number" value="{{$data->t_mobile}}">
		</div>
		<div class="form-group">
			<label>Teacher Gender</label>
			<input type="text" name="t_gender" placeholder="Teacher Gender" value="{{$data->t_gender}}">
		</div>
		<div class="form-group">
			<label>Teacher Designation</label>
			<input type="text" name="t_designation" placeholder="Teacher Designation" value="{{$data->t_designation}}">
		</div>
		<div class="form-group">
			<label>Address</label>
			<textarea name="t_address" placeholder="Teacher Address">{{$data->t_address}}</textarea>
			
		</div>

	

		<div class="form-group">
		<input type="submit" value="Update">
	</div>

		
	</form>
</div>
</div>




@endsection