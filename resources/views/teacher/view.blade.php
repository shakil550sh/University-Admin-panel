@extends('frontend.master')
@section('title','Teacher Profile')
@section('content')
	<div align="center" class="main-wrapper">
	<h1>View Teachers Profile</h1>
	<h4><a href="/teacher/create">Add More Teacher</a></h4>

	<table style="border: 2px solid red" align="center" border="1">
		<tr>
			<th>SI</th>
			<th>teacher name</th>
			<th>teacher code</th>
			<th>teacher mobile</th>
			<th>teacher gender</th>
			<th>teacher designation</th>
			<th>teacher address</th>
			<th>teacher Department</th>
			<th>Action</th>
		</tr>
		@foreach($tech as $key=>$data)
			
		<tr>
			<th>{{++$key}}</th>
			<th>{{$data->t_name}}</th>
			<th>{{$data->t_code}}</th>
			<th>{{$data->t_mobile}}</th>
			<th>{{$data->t_gender}}</th>
			<th>{{$data->t_designation}}</th>
			<th>{{$data->t_address}}</th>
			<th>{{$data->dpt_name}}</th>
			<th><a href="/teacher/{{$data->id}}/edit">Edit</a> | <a href="/teacher/delete/{{$data->id}}">Delete</a></th>
		</tr>
		@endforeach
	
	</table>
	

	</div>
	

</body>
</html>
@endsection