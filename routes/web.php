<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('AdminHome');
});
// for Departments
Route::get('/departments/create','DepartmentController@create');
Route::post('/departments','DepartmentController@store');
Route::get('/departments','DepartmentController@index');
Route::get('/departments/delete/{id}','DepartmentController@destroy');
Route::get('/departments/{id}/edit','DepartmentController@edit');
Route::post('/departments/{id}','DepartmentController@update');

//for Teachers
Route::get('/teacher/create','TeacherController@create');
Route::post('/teacher','TeacherController@store');
Route::get('/teacher','TeacherController@index');
Route::get('/teacher/delete/{id}','TeacherController@destroy');
Route::get('/teacher/{id}/edit','TeacherController@edit');
Route::post('/teacher/{id}','TeacherController@update');

//for Cource
Route::get('/cources/create','CourceController@create');
Route::post('/cources','CourceController@store');
Route::get('/cources','CourceController@index');
Route::get('/cources/delete/{id}','CourceController@destroy');
Route::get('/cources/{id}/edit','CourceController@edit');
Route::post('/cources/{id}','CourceController@update');

//for Students
Route::get('/student/create','StudentController@create');
Route::post('/student','StudentController@store');
Route::get('/student','StudentController@index');
Route::get('/student/delete/{id}','StudentController@destroy');
Route::get('/student/{id}/edit','StudentController@edit');
Route::post('/student/{id}','StudentController@update');

//Student view
Route::get('/studentview','Backend\StudentController@view');
Route::get('/jsonStudents','Backend\StudentController@viewStudent');

//Departments->student->Cource->Techer asign table
Route::get('/assignview','Backend\AssignController@view');
Route::get('/jsonStudents','Backend\AssignController@dptassignstu');
Route::get('/jsonCource','Backend\AssignController@stuassigncource');
Route::get('/jsonTecher','Backend\AssignController@courceassigntecher');

//Libary Section
Route::get('/book/create','BookController@create');
Route::post('/book','BookController@store');
Route::get('/book','BookController@index');
Route::get('/book/delete/{id}','BookController@destroy');
Route::get('/book/{id}/edit','BookController@edit');
Route::post('/book/{id}','BookController@update');


Route::get('/json-dpt_book','BookController@dptBook');

Route::get('/result/create','ResultController@create');

//Semester Section

Route::get('/semesteradd/create','SemesterController@create');
Route::post('/semesteradd','SemesterController@store');
Route::get('/semesteradd','SemesterController@index');
